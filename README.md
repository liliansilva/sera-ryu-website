# Sera Ryu Website


* Configurar o Git no windows (Nunca usei esse software, se o passo-a-passo tiver errado *shrug*):

1. Instalar Git no pc: https://gitforwindows.org/ 

2. Configure seu git

    a. No terminal do GitBash digite <pre>git config --global user.name "YOUR_USERNAME"</pre>

    b. Agora configure seu email <pre>git config --global user.email "your_email_address@example.com"</pre>

    c. Confira se está tudo ok <pre>git config --global --list</pre>

3. Crie uma pasta para manter seus arquivos de desenvolvimento e navegue até lá pelo terminal. <pre>cd {Caminho da pasta/Arraste a pasta aqui}</pre>

4. Clone o repositório do site 
    <pre>git clone https://gitlab.com/liliansilva/sera-ryu-website.git</pre> 
    <pre>cd sera-ryu-website</pre>

5. Coloque pra usar o branch master <pre>git checkout master</pre>

6. Pegue as atualizações mais recentes <pre>git pull origin master</pre>

7. Faça as alterações que você quiser no editor que você preferir (VS Code). <pre>code .</pre>

8. Quando terminar, dê o commit com o comando: 
    <pre>git add .</pre>
    <pre>git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"</pre>
    Usa sempre o push mesmo ;-;
    <pre>git push origin master</pre>


* OBS: Essas são instruções só pra iniciar. Se for continuar algum trabalho, comece usando as instruções do 6 pra frente.


# TO DO:

* Ingryd:

    1. Estudar alguns frameworks (Bootstrap, UIKit, Bulma, etc).

    2. Montar a estrutura do site utilizando um desses frameworks (só a estrutura, não precisa dos detalhes).

* Aline:

    1. Embed Twitter timeline -> https://help.twitter.com/en/using-twitter/embed-twitter-feed

    2. Embed YouTube feed -> https://embedsocial.com/knowledge-base/embed-youtube-feed/

* Lilian:

    1. Verificar opções de hospedagem.

    2. Validar estrutura do site.

    3. Validar códigos do Twitter/Youtube.

